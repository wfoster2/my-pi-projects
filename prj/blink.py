import RPi.GPIO as GPIO
from time import sleep

GPIO.setmode(GPIO.BCM)

p = 24

GPIO.setup(p, GPIO.OUT);

while (1):
    GPIO.output(p, True)
    sleep(.5)
    GPIO.output(p, False)
    sleep(.5)